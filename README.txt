Module: Google Consumer Surveys

Description
===========
Adds Google Consumer Surveys to your website.
Note: you must have an account on Google Consumer Surveys to use this module.
More info: https://www.google.com/insights/consumersurveys/home

Compatible with:
Context

Installation
============
Put the modules into your sites/module folder
Enable the module and set user permissions

Configuration
=============
Visit /admin/config/system/google_surveys
or via menus: Configuration > System > Google Consumer Surveys

Enter your Account Id and the Google Consumer Surveys URL.

If Context module is enabled, a context named google_surveys will be created and
it will be visible to all node types. You can modify the context to be more 
specific, i.e. restrict the surveys to show only for Articles.

Testing
=======
Visit a node in full view mode. 
You should see the survey on the main content area.
