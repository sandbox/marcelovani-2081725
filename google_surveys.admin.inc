<?php

/**
 * @file
 * Module configurations.
 */

/**
 * Form callback for admin/config/services/google_surveys.
 */
function google_surveys_settings_form($form, &$form_state) {
  $form['google_surveys_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Surveys Account ID'),
    '#default_value' => variable_get('google_surveys_account_id', ''),
    '#description' => t('Enter the Google Surveys Account ID. You must have a google surveys account for this module to work.'),
  );

  $form['google_surveys_script_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Surveys Script URL'),
    '#default_value' => variable_get('google_surveys_script_url', 'survey.g.doubleclick.net'),
    '#description' => t('Enter the Google Script URL. Google will have provided you with a URL for your script.
    If you are in testing mode it might be something like "clients4.google.com". Otherwise, it will be something
    like "survey.g.doubleclick.net". No leading or trailing slashes or other querystring items here please.'),
  );

  return system_settings_form($form);
}
