<?php

/**
 * @file
 * Context plugin.
 */

/**
 * Base class for a context reaction.
 */
class GoogleSurveys extends context_reaction {
  /**
   * Options form.
   */
  function options_form($context) {
    $form = array();
    $form['google_surveys'] = array(
      '#markup' => t('Google Survey scripts will be added to the Body on the main Content'),
    );
    $form['enabled'] = array(
      '#type' => 'value',
      '#value' => TRUE,
    );
    return $form;
  }

  /**
   * Execute.
   */
  function execute(&$value) {
    if (!empty($value)) {
      $contexts = context_active_contexts();
      foreach ($contexts as $context) {
        if (!empty($context->reactions['google_surveys'])) {
          $value = _google_surveys_add_js($value);
        }
      }
    }
  }
}
