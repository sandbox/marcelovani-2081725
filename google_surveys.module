<?php

/**
 * @file
 * Google Consumer Surveys module.
 */

/**
 * Implements of hook_menu().
 */
function google_surveys_menu() {
  $items = array();
  $items['admin/config/system/google_surveys'] = array(
    'title' => 'Google Consumer Surveys',
    'description' => 'Adjust the settings for Google Surveys.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('google_surveys_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'google_surveys.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_node_view_alter().
 */
function google_surveys_node_view_alter(&$build) {
  $value = &$build['body'][0]['#markup'];
  if (variable_get('google_surveys_account_id', '') == '') {
    drupal_set_message(t('Google Consumer Surveys Account ID is missing, please check the configuration.'), 'warning');
  }
  else {
    // Only add script on full node view mode.
    if ($build['#view_mode'] == 'full') {
      /* If Context module is enabled, it's possible to specify a condition to
       * refine when the script should be added to the page.
       * i.e. only on certain node types.
       */
      if (module_exists('context')) {
        if ($plugin = context_get_plugin('reaction', 'google_surveys')) {
          $plugin->execute($value);
        }
      }
      else {
        // If Context module is not enabled, add the script to all node types.
        $value = _google_surveys_add_js($value);
      }
    }
  }
}

/**
 * Helper to generate the JS.
 */
function _google_surveys_add_js($value) {
  // See https://support.google.com/consumersurveys/answer/3333507.
  $script_url = variable_get('google_surveys_script_url', 'survey.g.doubleclick.net');
  $account_id = urlencode(variable_get('google_surveys_account_id', ''));

  // Build the script.
  $pre_js  = "<script type=\"text/javascript\"> (function() {" . PHP_EOL;
  $pre_js .= "var ARTICLE_URL = window.location.href;" . PHP_EOL;
  $pre_js .= "var CONTENT_ID = 'everything'; document.write(" . PHP_EOL;
  $pre_js .= "'<scr'+'ipt '+ 'src=\"//$script_url/survey?site=$account_id'+ '&amp;url='+encodeURIComponent(ARTICLE_URL)+" . PHP_EOL;
  $pre_js .= "(CONTENT_ID ? '&amp;cid='+encodeURIComponent(CONTENT_ID) : '')+ '&amp;random='+(new Date).getTime()+" . PHP_EOL;
  $pre_js .= "'\" type=\"text/javascript\">'+'\x3C/scr'+'ipt>');" . PHP_EOL;
  $pre_js .= "})(); </script>" . PHP_EOL;

  $pos_js = '<script type="text/javascript"> try { _402_Show(); } catch(e) {}; </script>';

  // Wrap the value.
  $value = $pre_js . '<div class="p402_premium">' . $value . '</div>' . $pos_js;
  return $value;
}

/**
 * Implements hook_context_plugins().
 */
function google_surveys_context_plugins() {
  $plugins = array();
  $plugins['google_surveys'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'google_surveys') . '/plugins',
      'file' => 'google_surveys_context_reaction_script.inc',
      'class' => 'GoogleSurveys',
      'parent' => 'context_reaction',
    ),
  );
  return $plugins;
}

/**
 * Implements hook_context_registry().
 */
function google_surveys_context_registry() {
  $registry['reactions'] = array(
    'google_surveys' => array(
      'title' => t('Google Surveys'),
      'description' => t('Add Google Surveys scripts on the page.'),
      'plugin' => 'google_surveys',
    ),
  );
  return $registry;
}
